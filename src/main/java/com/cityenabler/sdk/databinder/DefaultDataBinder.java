package com.cityenabler.sdk.databinder;

import java.util.Optional;

import com.cityenabler.sdk.databinder.model.DataBinder;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;







@SuppressWarnings("deprecation")
public class DefaultDataBinder implements DataBinder {
	
	private ObjectMapper objectMapper = new ObjectMapper();
	
	@Override
	public void init() {
		objectMapper.setSerializationInclusion(Include.NON_NULL);
		objectMapper.setDateFormat(new ISO8601DateFormat());
		//Support for JDK8 Types
		objectMapper.registerModule(new Jdk8Module());
	}
	
	@Override
	public <T> String toJson(T entity) {
		String out = "{}";
		try { out = objectMapper.writeValueAsString(entity); }
		catch (Exception e) {
			e.printStackTrace();
		}
		return out;
	}

	@Override
	public <T> T toEntity(String json, Class<T> clazz) {
		Optional<T> out = Optional.empty();
		try {
			//Do the deserialization with Jackson
			out = Optional.of(objectMapper.readValue(json.getBytes(), clazz)); }
		catch (Exception e) {
			//In case of errors in the Deserialization returns with the empty Optional entity
			e.printStackTrace();
		}
		
		//If the Optional has a value, returns it; otherwise returns null
		return out.orElse(null);
	}


}
